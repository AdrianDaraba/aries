'use strict'

const User = require('../models/users');

module.exports = {
    createUser: createUser,
    getUsers: getUsers,
    getUsersById: getUsersById,
    deleteUsersById: deleteUsersById,
    responseToJSON: responseToJSON
};

function createUser(req, res, next) {
    const addUser = req.body;    
    addUser.details = JSON.parse(addUser.details);
    addUser.documents = JSON.parse(addUser.documents);
    
    console.log('req.body', req.body);
    //console.log('addUser', addUser);
    const user = new User(addUser);
    console.log('user', user);
    user.save(function(err, result){
        if(err){
            return res.json(err);
        }        
        req.resources.addUser = result;
        console.log('request', req.resources.addUser)
        return next();      
    })
}

function getUsers(req, res, next) { 
    let filter = req.query.role;
    User.find({"details.role": filter}, function(err, result) {
        if(err){
            return res.json(err);
        }
        req.resources.users = result;
        return next();
    })
}

function getUsersById(req, res, next) {  
    //req.param {}   -  param dinn routes
    //req.query {}  - param cu ? din url
    filters = req.query
    console.log('param', req.params)
    User.find({_id: req.params.userId}, function(err, result) {
        if(err){
            return res.json(err);
        }
         req.resources.addUser = result; 
         console.log('dasdasas', result)     
         return next();
    })
}

function deleteUsersById(req, res, next) {  
    User.deleteOne({_id: req.params.userId}, function(err, result) {
        if(err){
            return res.json(err);
        }
        req.resources.addUser = result;
        return next();       
    })
}

function responseToJSON(prop) {
    return function(req, res, next) {
        return res.json(req.resources[prop]);
    }
}