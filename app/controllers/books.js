'use strict'

const Book = require('../models/books');

module.exports = {
        createBook: createBook,
        getBooks: getBooks,
        getBookById: getBookById,
        deleteBookById: deleteBookById,
        responseToJSON: responseToJSON
    };

    function createBook(req, res, next) {
        const book = new Book(req.body);
        book.save(function(err, result){
            if(err){
                //return res.json(err);
            
                return next({test: 2}); 
            }        
            req.resources.addBook = result;
            return next();      
        })
    }
    
    function getBooks(req, res, next) {
        /*       
        Book.find(function(err, result) {
            if(err){
                return res.json(err);
            }
            req.resources.books = result;
            return next();     
        })
        */
       Book
        .find()
        .sort({title: -1})
        .populate('user', 'email name documents.name documents.docType')
        .exec(function(err, result) {
            if(err){
                return res.json(err);
            }
            req.resources.books = result;
            return next();     
        })
    }
    
    function getBookById(req, res, next) {
        Book.find({_id: req.params.bookId}, function(err, result) {
            if(err){
                return res.json(err);
            }
             req.resources.addBook = result;   
             return next();
        })
    }
    
    function deleteBookById(req, res, next) {
        Book.deleteOne({_id: req.params.bookId}, function(err, result) {
            if(err){
                return res.json(err);
            }
            req.resources.addBook = result;
            return next();       
        })
    }
    
    function responseToJSON(prop) {
        return function(req, res, next) {
            return res.json(req.resources[prop]);
        }
    }


