"use strict";

const multer = require('multer');
const path = require('path');
const config = require('../../config/index')

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
      //let pathPrefix = path.resolve(__dirname, "../files");
      console.log("!!destination file", file);
      cb(null, config.uploadPath);
    },
    filename: function (req, file, cb) {
      console.log("!!file name file", file);
      cb(null, `${Date.now()}-${file.originalname}`);
    },
  });

var upload = multer({ storage: storage });

function getFile(req, res, next) {
  let filename = req.query.filename;
  let pathPrefix = path.resolve(__dirname, `../files/${filename}`);
  res.download(pathPrefix);
}

module.exports = {
  storage: storage,
  upload: upload,
  getFile: getFile
};