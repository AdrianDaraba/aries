'use strict'

const express = require('express');
const router = express.Router();
const userCtrl = require('../controllers/users');

router.get('/users', 
    userCtrl.getUsers,
    userCtrl.responseToJSON("users")  
);

router.get('/users/:userId', 
    userCtrl.getUsersById,
    userCtrl.responseToJSON("addUser")    
);

router.post('/users',
    userCtrl.createUser,
    userCtrl.responseToJSON("addUser")
);

router.delete('/deleteUser/:userId', 
    userCtrl.deleteUsersById,
    userCtrl.responseToJSON('addUsers')
);

module.exports = router;