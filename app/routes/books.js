'use strict'

const express = require('express');
const router = express.Router();
const bookCtrl = require('../controllers/books');

router.get('/books', 
    bookCtrl.getBooks,
    bookCtrl.responseToJSON("books")  
);

router.get('/books/:bookId', 
    bookCtrl.getBookById,
    bookCtrl.responseToJSON("addBook")    
);

router.post('/books',
    bookCtrl.createBook,
    bookCtrl.responseToJSON("addBook")
);

router.delete('/deleteBook/:bookId', 
    bookCtrl.deleteBookById,
    bookCtrl.responseToJSON('addBook')
);

module.exports = router;