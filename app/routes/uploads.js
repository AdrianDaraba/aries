'use strict'

const express = require('express');
const router = express.Router();
const uploadCtrl = require('../controllers/upload')

router.post('/uploads',
    uploadCtrl.upload.single('avatar'),
        function(req, res, next) {
            console.log('route file', req.file);
            console.log('filename: ', req.file.fieldname)
            return res.json({fileName: req.file.fieldname});
        }
);

router.get('/donwload-file',
  uploadCtrl.getFile
  );

module.exports = router;