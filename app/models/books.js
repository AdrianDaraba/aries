'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = mongoose.ObjectId;

const userSchema = new Schema ({
    createAt: Number,
    updateAt: Number,
    title: {
        type: String,
        required: [true, 'description field is required'],
        unique: false
       
    },
     author: {
         type: String,
         required: true,
         unique: false  
     },
     price: {
         type: Number
     },
     user: {
         type: ObjectId,
         ref: 'user',
         required: true
     }
},
{
    timestamps: { currentTime: () => new Date().getTime() }  
});

module.exports = mongoose.model('book', userSchema, 'books');