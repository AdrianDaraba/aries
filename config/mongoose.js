module.exports = {
    initMongoose: initMongoose
}

const mongoose=require('mongoose');
const config=require('./index');

function initMongoose() {
    mongoose.connect(config.mongoUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
}

/*
const db = mongoose.connection;

db.on('error', function() {
    console.log('error')
});

db.once('open', function() {
    console.log('connect');
});
*/
