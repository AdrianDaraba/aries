'use strict'

const bodyParser = require('body-parser');

module.exports = {
    initExpress: initExpress
};

function initExpress(app) {

    //parse app/json
    app.use(bodyParser.json());

    app.use(function(req, res, next) {
        console.log('!!!req.', req.resources);
        req.resources = req.resources || {};
        next();
    });
}