'use strict'

const express = require('express');
const app = express();
const config = require('./config/index');

require('./config/mongoose').initMongoose();
require('./config/express').initExpress(app);
require('./config/security').initHelmet(app);
require('./config/routes').initRoutes(app);

app.all('*', function(req, res, next){
    return res.status(404).json({
        status: 'fail',
        message: `Can't find ${req.url} on this server`
    });
});

app.use(function(err, req, res, next) {
    console.log('err middleware');
    return res.status(err && err.statusCode || 400).json({
        status: 'error',
        message: err && err.message || 'Default message'
    });
});

app.listen(config.PORT, function() {
    console.log(`API on port ${config.PORT}`);
});